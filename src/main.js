import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config'

import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css'                           //icons
import '/node_modules/primeflex/primeflex.css'
import '@iconify/vue'
import BadgeDirective from 'primevue/badgedirective';

const app = createApp(App)

app.use(router)
app.use(PrimeVue)
app.directive('badge', BadgeDirective);

app.mount('#app')
