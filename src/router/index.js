import { createRouter, createWebHistory } from 'vue-router'
import PlaceList from '../views/PlaceList.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'PlaceList',
      component: PlaceList
    }
  ]
})

export default router;
